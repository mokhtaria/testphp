<?php

namespace App\Tests\Entity;

use App\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StudentTest extends KernelTestCase
{

    private ValidatorInterface $validator;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->validator = $kernel->getContainer()->get('validator');
    }
    public function getEntity(): Student
    {
        $student = new Student();

        $student
            ->setFirstname('Firsname')
            ->setLastname('LastName')
            ->setEnumEtud('1234567892');
        return $student;
    }

    public function assertHasErrors(Student $student, int $number = 0)
    {
        self::bootKernel();
        $errors = $this->validator->validate($student);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }


    /**
     * Test if User is a valid entity
     */
    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * test if empty firstname
     */
    public function testEmptyEmail()
    {
        $this->assertHasErrors($this->getEntity()->setFirstname(''), 1);
    }

    /**
     * test if empty lastname
     */
    public function testEmptyAvatar()
    {
        $this->assertHasErrors($this->getEntity()->setLastname(''), 1);
    }

}
