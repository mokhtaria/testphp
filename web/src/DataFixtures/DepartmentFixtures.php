<?php

namespace App\DataFixtures;

use App\Entity\Department;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;


class DepartmentFixtures extends Fixture
{
    protected $faker;

    public function load(ObjectManager $manager)
    {
        $departments = [];
        for ($i = 0; $i < 10; $i++) {
            $departments[$i] = new Department();
            $departments[$i]->setName($this->faker->name);
            $departments[$i]->setCapacity($this->faker->capacity);
            $manager->persist($departments[$i]);
        }

        $manager->flush();
    }
}
