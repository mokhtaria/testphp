<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

class Department
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 25)]
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 25,
        maxMessage: 'le nombre de caractère du nom dépasse {{ limit }} caractères.',
    )]
    private $name;

    #[ORM\Column(type: 'integer', length: 10)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 10,
        max: 10,
        maxMessage: 'le nombre de chiffre du etudNum doit étre {{ limit }} chiffres.',
    )]
    private $capacity;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    public function __toString()
    {
        return $this->name;
    }
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCapacity(): ?string
    {
        return $this->capacity;
    }

    public function setCapacity(?string $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }
}
