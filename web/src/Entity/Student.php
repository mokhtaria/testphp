<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StudentRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity(repositoryClass:StudentRepository ::class)]
#[ApiResource(
    collectionOperations: [
        'get' => ['normalization_context' => ['groups' => 'student:list']],
        ],
    itemOperations: ['get' => ['normalization_context' => ['groups' => 'student:item']]],
    order: ['id' => 'DESC'],
    paginationEnabled: false
)]
class Student
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 25)]
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 25,
        maxMessage: 'le nombre de caractère du nom dépasse {{ limit }} caractères.',
    )]
    #[Groups(['student:list', 'student:item'])]
    private $firstname;

    #[ORM\Column(type: 'string', length: 25)]
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 25,
        maxMessage: 'le nombre de caractère du prenom dépasse {{ limit }} caractères.',
    )]
    #[Groups(['student:list', 'student:item'])]
    private $lastname;

    #[ORM\Column(type: 'integer', length: 10)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 10,
        max: 10,
        maxMessage: 'le nombre de chiffre du etudNum doit étre {{ limit }} chiffres.',
    )]
    #[Groups(['student:list', 'student:item'])]
    private $enumEtud;

    #[ORM\ManyToOne(targetEntity: self::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['student:list', 'student:item'])]
    private $department;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEnumEtud():int
    {
        return $this->enumEtud;
    }

    public function setEnumEtud($enumEtud): void
    {
        $this->enumEtud = $enumEtud;
    }

    public function getDepartment():? Department
    {
        return $this->department;
    }

    public function setDepartment(Department $department): void
    {
        $this->department = $department;
    }
}
