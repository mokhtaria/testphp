<?php

namespace App\Form;

use App\Entity\Student;
use App\Entity\Department;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class StudentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TelType::class, [
                'required' => true,
                'label' => "FirstName",
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un nom'
                    ]),
                    new Length([
                        'max' => 25,
                        'minMessage' => 'Le nom doit contenir au maximum {{ limit }} caractères'
                    ]),
                ]
            ])
            ->add('lastName', TextareaType::class, [
                'required' => true,
                'label' => "LastName",
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un prénom'
                    ]),
                    new Length([
                        'max' => 25,
                        'minMessage' => 'Le nom doit contenir au maximum {{ limit }} caractères'
                    ]),
                ]
            ])
            ->add('numEtud', IntegerType::class, [
                'required' => true,
                'label' => "NumEtud",
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un numéro étude'
                    ]),
                    new Length([
                        'min' => 10,
                        'max' => 10,
                        'minMessage' => 'Le nom doit contenir au minimum {{ limit }} chiffres'
                    ]),
                ]
            ])
            ->add('department', EntityType::class, [
                'required' => true,
                'label' => 'Choisir une ville',
                'class' => Department::class,
                'choice_label' => 'Department',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un departement'
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
