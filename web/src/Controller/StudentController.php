<?php

namespace App\Controller;

use App\Entity\Student;
use App\Form\StudentType;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StudentController extends AbstractController
{
    public function __construct(public StudentRepository $studentRepository,public EntityManagerInterface $em)
    {
    }

    #[Route('/students', name: 'app_students')]
    public function index(): Response
    {
        $students = $this->studentRepository->findAll();

        return  $this->render('student/index.html.twig', [
            'students' => $students
        ]);
    }

    #[Route('/student/new', name: 'app_new_student')]
    #[Route('/student/{id}/edit', name: 'app_edit_student')]
    public function form(Request $request,Student $student = null)
    {
        if(!$student){
            $student = new Student();
        }

        $form = $this->createForm(StudentType::class, $student);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->em->persist($student);
            $this->em->flush();
            $this->addFlash('success', 'Student Updated!');
            return $this->redirectToRoute('app_students');
        }
        return $this->render('student/add.html.twig', [
            'formStudent' => $form->createView(),
            'editMode' =>$student->getid() !== null,
            'id' =>$student->getid() !== null ?$student->getid():null
        ]);
    }

    #[Route('/student/delete/{id}', name: 'delete_student')]
    public function delete(Student $student): Response {

        $this->em->remove($student);
        $this->em->flush();

        $this->addFlash('deleteArticle', 'Student a bien étais supprimer');

        return $this->redirectToRoute('app_students');
    }

    #[Route('/list/{department}', name: 'list_student_department')]
    public function listStudentBydepartment(StudentRepository $studentRepository,$department): Response
    {
        $students = $studentRepository->findBy(['id'=>$department]);

        return $this->render('student/index.html.twig', ['students' => $students]);
    }
}
